# README #

This repository contains a Coloured Petri net model developed with the CPN Tools (http://cpntools.org/) for the MQTT protocol.

To properly be able to use the model and use the regular CPN Tools functionalities, one must set the own directory location URL of this repository from the computer into the "modelpath" variable (see figure below):

![alt text](https://i.imgur.com/rXBlW5Q.png "modelpath variable")
